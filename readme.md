## Add These Lists To Your Ad Blocker

- [Click here to add Amazon Annoyances](https://subscribe.adblockplus.org/?location=https://gitlab.com/samuel.viltres/miscfilters/-/raw/master/amazonannoyances.txt)
- [Click here to add Anti-Paywall list](https://subscribe.adblockplus.org/?location=https://gitlab.com/samuel.viltres/miscfilters/-/raw/master/antipaywall.txt)
- [Click here to add Click To Load list](https://subscribe.adblockplus.org/?location=https://gitlab.com/samuel.viltres/miscfilters/-/raw/master/clicktoload.txt)
- [Click here to add Facebook list](https://subscribe.adblockplus.org/?location=https://gitlab.com/samuel.viltres/miscfilters/-/raw/master/facebook.txt)
- [Click here to add Yahoo list](https://subscribe.adblockplus.org/?location=https://gitlab.com/samuel.viltres/miscfilters/-/raw/master/yahoo.txt)
